
import json
from textwrap import indent
from genie.conf import Genie
from genie.testbed import load
from pprint import pprint

ospf={}
testbed=load("./inventory.yml")
tb = Genie.init(testbed)
for device in testbed.devices.values():
    print(f'Connecting to {device.name}')
    
    try:
        device.connect(log_stdout=False)
        out= device.parse('show ip route')
        
        p=out['vrf']['default']['address_family']['ipv4']['routes']
        # print(json.dumps(p,indent=4))
        for k, v in p.items():

            if v["source_protocol"] == 'ospf':
                print(k)

                tmp={}
                for a,b in v['next_hop']['next_hop_list'].items():

                    tmp[b['next_hop']]=b['outgoing_interface']
                ospf[k]=tmp
            else:
                continue

      
    except Exception as e:
        print(e)

          # p=out['instance']['default']['vrf']['default']
   
print(ospf)     
        # if "neighbor" in p.keys():
        #     for nei ,data in p['neighbor'].items():
        #         print(f"{device.name} has OSPF neighbor with nei {nei}  {data['session_state']}")