import asyncio
import json
import networkx as nx
import matplotlib.pyplot as plt
from logic import Device

interface_list = {}
lldp_list = {}
lldp_link = []
ospf_link = {}
ospf_list = []

tmp_interface_list_dict = {}
devices = ['192.168.61.1', '192.168.61.2', '192.168.61.3', '192.168.61.4', "192.168.61.5"]

async def process_device(device):
    D = Device(device)
   
    await D.get_interfaces_all()
    await D.get_ospf_neighbors()
    await D.get_lldp_all()
    
    print(D.ospf_routes)
    ospf_link[D.host] = D.ospf_neighbors
    lldp_list[D.host] = D.lldp_neighbors
    
    interfaces = D.Interface_list
    tmpl = []
    for entry in D.Interface_list:
        if "ip" in entry.keys():
            tmpl.append(entry['ip'])
        
    tmp_interface_list_dict[D.host] = tmpl

async def main():
    tasks = [process_device(device) for device in devices]
    await asyncio.gather(*tasks)
    
    for k, v in ospf_link.items():
        for i in v[0].keys():
            for a, b in tmp_interface_list_dict.items():
                for s in b:
                    if s == i:
                        link = {
                            'SourceNode': k,
                            'DestNode': a
                        }
                        if {'SourceNode': a, 'DestNode': k} not in ospf_list:
                            ospf_list.append(link)
    
    for k, v in lldp_list.items():
        link = {
            'SourceNode': k,
            'DestNode': v[0]['name']
        }
        if {'SourceNode': v[0]['name'], 'DestNode': k} not in lldp_link:
            lldp_link.append(link)
    
    print('lldp')
    for i in lldp_link:
        print(i)
    print('ospf')
    for i in ospf_list:
        print(i)
    
    G = nx.Graph()
    for key in interface_list.keys():
        G.add_node(key)
    
    for i in ospf_list:
        G.add_edge(i['SourceNode'], i['DestNode'])
    
    nx.draw(G, with_labels=True, node_size=3000)
    plt.show()

asyncio.run(main())