

from os import device_encoding
from this import s
import requests
import json
from requests.auth import HTTPBasicAuth
from urllib3 import disable_warnings
from pprint import pprint
from genie.conf import Genie
from genie.testbed import load
import yaml
from netmiko.ssh_autodetect import SSHDetect
from netmiko.ssh_dispatcher import ConnectHandler

class Device(object):

    
    def __init__(self,device:str):
        self.device=device
        self.Interface_list=[]
        self.lldp_neighbors=[]
        self.ospf_beighbors=[]
        self.bgp_neighbors=[]
        self.host=''
        self.ios=''
        self.ospf_routes={}
    def get_interfaces_all(self):
        self.host_details()
        self.prep_inventory()
        
        testbed=load("./inventory1.yml")
        tb = Genie.init(testbed)
        for device in tb.devices.values():
            print(f'Collecting Interfaces from {device.name}')
            
            try: 
                device.connect(log_stdout=False)
                out= device.learn('interface').info
                # print(json.dumps(out,indent=4))
                for k,v in out.items():
                    interface={}
                    interface['name']=k
                    interface['type']=v['type']
                    interface['status']=v['oper_status']
                    interface['mac']=v['phys_address']
                    interface['enabled']=v['enabled']
                    interface['in-pckts']=v['counters']['in_pkts']
                    interface['out-pckts']=v['counters']['out_pkts']
                    interface['in-errors']=v['counters']['in_errors']
                    interface['out-errors']=v['counters']['out_errors']
                    
                    if "ipv4" in v.keys():
                        for a,b in v['ipv4'].items():
                            
                            interface['ip']=b['ip']
                            interface['prefix']=b['prefix_length']
                    self.Interface_list.append(interface)
                    


            except Exception as e:
                continue
        return self.Interface_list

    # def get_interface(self):
    #     disable_warnings()
    #     global Interface_list
    #     url=f'https://{self.device}/restconf/data/ietf-interfaces:interfaces'
    #     headers={
    #         "Accept": "application/yang-data+json",
    #         "Content-type":'application/yang-data+json'
    #     }
    #     cred=HTTPBasicAuth(
    #         username='admin',
    #         password='cisco'
    #     )
    #     result=requests.get(
    #         url=url,
    #         headers=headers,
    #         auth=cred,
    #         verify=False
    #     )
        
    #     for entry in result.json()['ietf-interfaces:interfaces']['interface']:
    #         interface={}
    #         if 'address' in entry['ietf-ip:ipv4'].keys() :
    #             IP=entry['ietf-ip:ipv4']['address'][0]['ip']
    #             MASK=entry['ietf-ip:ipv4']['address'][0]['netmask']
    #         else:
    #             IP="N/A"
    #             MASK='N/A'
    #         interface['name']=entry['name']
    #         interface['type']=entry['type']
    #         interface['IP']=IP
    #         interface['mask']=MASK
    #         self.Interface_list.append(interface)
        # return self.Interface_list

    # def get_interface_status(self):
    #     disable_warnings()
    #     for i,entry in enumerate(self.Interface_list):
    #         url=f'https://{self.device}/restconf/data/ietf-interfaces:interfaces-state/interface={entry["name"]}'
    #         headers={
    #             "Accept": "application/yang-data+json",
    #             "Content-type":'application/yang-data+json'
    #         }
    #         cred=HTTPBasicAuth(
    #             username='admin',
    #             password='cisco'
    #         )
    #         result=requests.get(
    #             url=url,
    #             headers=headers,
    #             auth=cred,
    #             verify=False).json()['ietf-interfaces:interface']
    #         self.Interface_list[i]['admin-status']=result['admin-status']
    #         self.Interface_list[i]['oper-status']=result['oper-status']
    #         self.Interface_list[i]['MAC']=result['phys-address']
    #         self.Interface_list[i]['in-traffic']=result['statistics']['in-octets']
    #         self.Interface_list[i]['out-traffic']=result['statistics']['out-octets']
    #         self.Interface_list[i]['in-errors']=result['statistics']['in-errors']
    #         self.Interface_list[i]['out-errors']=result['statistics']['out-errors']
    #         self.Interface_list[i]['in-discards']=result['statistics']['in-discards']
    #         self.Interface_list[i]['out-discards']=result['statistics']['out-discards']
            
    def get_lldp_all(self):
        self.host_details()
        self.prep_inventory()
        lldp={}
        testbed=load("./inventory1.yml")
        tb = Genie.init(testbed)
        for device in tb.devices.values():
            print(f'Collecting LLDP From {device.name}')
            
            try: 
                device.connect(log_stdout=False)
                out= device.learn('lldp').info
                # print(json.dumps(out,indent=4))
                for k,v in out['interfaces'].items():
                    if "neighbors" in v['port_id'][k].keys():
                        lldp['connected-via']=k
                        for a,b in v['port_id'][k]['neighbors'].items():
                            lldp['name']=str(a).rsplit('.')[0]
                            lldp['neighbor-ip']=b['management_address']
                        

                        self.lldp_neighbors.append(lldp)

            except Exception as e:
                continue
        return self.lldp_neighbors

    # def get_lldp_neighbors(self):
    #     nei={}
    #     for i,entry in enumerate(self.Interface_list):
            
    #         try: 
    #             url=f'https://{self.device}/restconf/data/openconfig-lldp:lldp/interfaces/interface={entry["name"]}/neighbors'
    #             headers={
    #                 "Accept": "application/yang-data+json",
    #                 "Content-type":'application/yang-data+json'
    #             }
    #             cred=HTTPBasicAuth(
    #                 username='admin',
    #                 password='cisco'
    #             )
    #             result=requests.get(
    #                 url=url,
    #                 headers=headers,
    #                 auth=cred,
    #                 verify=False).json()['openconfig-lldp:neighbors']
                
                
    #             for neighbor in result['neighbor']:
    #                 nei['name']=str(neighbor['state']['system-name']).rsplit('.')[0]
    #                 nei['connected-via']=neighbor['state']['port-description']
    #                 nei['neighbor-ip']=neighbor['state']['management-address']
                    
                
    #             # Interface_list[i]['neighbors']=nei
    #             self.lldp_neighbors.append(nei)
        
        
    #         except Exception:
    #             continue
       
       
    def host_details(self):
        device ={
            'device_type': 'autodetect',
            'host':self.device,
            'username':'admin',
            'password':'cisco'
        }
        guess=SSHDetect(**device)
        ios_c=guess.autodetect()
        host=ConnectHandler(
            device_type=ios_c,
            host=self.device,
            username='admin',
            password='cisco'
        )
        if ios_c =='cisco_ios':
            self.ios='ios'

        self.host=host.find_prompt().replace('#','')
        host.disconnect()

    def prep_inventory(self):
        self.host_details()
        f=open('./inventory.yml','r')
        doc=yaml.safe_load(f)
        doc['devices']['CE1']['connections']['default']['ip']=self.device
        doc['devices']['CE1']['os']=self.ios
        doc['devices'][self.host]=doc['devices'].pop('CE1')
        
        fc=open('./inventory1.yml','w')
        yaml.safe_dump(doc,fc)
        
       
            

    def get_ospf_neighbors(self):
        self.host_details()
        self.prep_inventory()
        ospf={}
        testbed=load("./inventory1.yml")
        tb = Genie.init(testbed)
        for device in tb.devices.values():
            print(f'Collecting OSPF from {device.name}')
            
            try: 
                device.connect(log_stdout=False)
                out= device.learn('ospf').info
                out2= device.parse('show ip route')
        
                pa=out2['vrf']['default']['address_family']['ipv4']['routes']
                p=out['vrf']['default']['address_family']['ipv4']['instance']['1']['areas']['0.0.0.0']['interfaces']
                
                for k, v in p.items():
                    if "neighbors" in v.keys():
                        for nei,data in v['neighbors'].items():
                            #print(f"{device.name} has OSPF neighbor with {data['neighbor_router_id']} on {k}  {nei}")
                            ospf[nei]=k
                self.ospf_beighbors.append(ospf)
                for ka, va in pa.items():

                    if va["source_protocol"] == 'ospf':
                        print(k)

                        tmp={}
                        for a,b in va['next_hop']['next_hop_list'].items():

                            tmp[b['next_hop']]=b['outgoing_interface']
                        self.ospf_routes[ka]=tmp
                    else:
                        continue
            except:
                print('Null')
                self.ospf_beighbors.append('Null')

    def get_bgp_neighbors(self):
        bgp={}
        testbed=load("./inventory1.yml")
        tb = Genie.init(testbed)
        for device in testbed.devices.values():
            print(f'Connecting to {device.name}')
            
            try:
                device.connect(log_stdout=False)
                out= device.learn('bgp').info
                p=out['instance']['default']['vrf']['default']
        
            
                if "neighbor" in p.keys():
                    for nei ,data in p['neighbor'].items():
                        #print(f"{device.name} has BGP neighbor with nei {nei}  {data['session_state']}")
                        bgp[nei]=data['session_state']
                    self.bgp_neighbors.append(bgp)

            except:
                print('Null')
                self.bgp_neighbors.append('Null')

    




   


    

if __name__=="__main__":
    # get_interface(device='192.168.0.233')
    # get_interface_status(device='192.168.0.233')
    # get_lldp_neighbors(device='192.168.0.233')
    # get_ospf_neighbors()
    # get_bgp_neighbors()
    CE1=Device('192.168.0.233')
    CE1.get_interface()
    CE1.prep_inventory()
    CE1.get_ospf_neighbors()
    print(CE1.ospf_beighbors)
    
    
   
   


