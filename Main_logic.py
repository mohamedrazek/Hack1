
from logic import Device
import json
import networkx as nx
import matplotlib.pyplot as plt

interface_list={}
lldp_list={}
lldp_link=[]
ospf_link={}
ospf_list=[]

tmp_interface_list_dict={}
devices=['192.168.0.23','192.168.0.233','192.168.0.189','192.168.0.195',"192.168.0.148"]

for device in devices:
    D=Device(device)
   
    D.get_interfaces_all()
    D.get_ospf_neighbors()
    D.get_lldp_all()
    print(D.ospf_routes)
    ospf_link[D.host]=D.ospf_beighbors
    lldp_list[D.host]=D.lldp_neighbors
    # print(json.dumps(D.lldp_neighbors,indent=4))

    interfaces=D.Interface_list
    tmpl=[]
    for entry in D.Interface_list:
        
        if "ip" in entry.keys():
            tmpl.append(entry['ip'])
        
    tmp_interface_list_dict[D.host]=tmpl
for k,v in ospf_link.items():
    for i in v[0].keys():
     for a,b in tmp_interface_list_dict.items():
        for s in b:
            if s ==i:
                link={
        'SourceNode': k,
        'DestNode':a
    }
                if {'SourceNode':a,'DestNode':k} not in ospf_list:
                    ospf_list.append(link)

    # D.get_ospf_neighbors()
    # ospf_link[D.host]=D.ospf_beighbors

   
    
    


for k,v in lldp_list.items():
    link={
        'SourceNode': k,
        'DestNode':v[0]['name']
    }
    if {'SourceNode':v[0]['name'],'DestNode':k} not in lldp_link:
        lldp_link.append(link)
    
print('lldp')
for i in lldp_link:
    print(i)
print('ospf')
for i in ospf_list:
    print(i)
G=nx.Graph()
for key in interface_list.keys():
    G.add_node(key)

for i in ospf_list:
    G.add_edge(i['SourceNode'],i['DestNode'])
nx.draw(G,with_labels=True,node_size=3000)
plt.show()


    




    