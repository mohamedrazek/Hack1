from netmiko.ssh_autodetect import SSHDetect
from netmiko.ssh_dispatcher import ConnectHandler
device ={
    'device_type': 'autodetect',
    'host':'192.168.0.195',
    'username':'admin',
    'password':'cisco'
}
guess=SSHDetect(**device)
match=guess.autodetect()
host=ConnectHandler(
    device_type='cisco_ios',
    host='192.168.0.189',
    username='admin',
    password='cisco'
)
name=host.find_prompt()
print(name.replace('#',''))
print(match)